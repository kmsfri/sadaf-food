<?php
namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use DB;

class HomeController extends Controller

{

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  public $selectedStoreType;

  function __construct()
  {
    $session = new Session();
    $selectedStoreType = $session->get('selected_store_type');
    if(!empty($selectedStoreType)){
      $this->selectedStoreType = $selectedStoreType;
    }else{
      $this->selectedStoreType = 12;
    }

  }



  public function showMainPage(Request $request)
  {

    if($this->selectedStoreType==12){
      $allCategoriesFoods = DB::table('BASProduct')
          ->where('Flg_restooran','1')
          ->where('Active','1')
          ->get();
    }else{
      $allCategoriesFoods = DB::table('BASProduct')
          ->where('Flg_boofeh','1')
          ->where('Active','1')
          ->get();
    }


    $foods = [];
    foreach($allCategoriesFoods as $foodItem){
      $toShowFoodCategories[$foodItem->ProductCtgry] = $foodItem->ProductCtgry;
      $foods[$foodItem->ProductCtgry][] = $foodItem;
    }


    $foodCategories = DB::table('BASLookup')
        ->where('Type','LIKE','%ProductCtgry%')
        ->where('Aux','LIKE','%'.$this->selectedStoreType.'%')
        ->where('Active','1')
        ->whereIn('Code',$toShowFoodCategories)
        ->get();


    $session = new Session();
    $customerCartSession = $session->get('customer_cart_session');

    $allCartElementsCount = 0;
    if(is_array($customerCartSession)){
      foreach($customerCartSession as $sessionItem){
        $allCartElementsCount += $sessionItem;
      }
    }

    return view('customer.pages.main',compact('foodCategories','foods','customerCartSession','allCartElementsCount'));
  }

}
