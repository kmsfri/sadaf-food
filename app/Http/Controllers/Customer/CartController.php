<?php
namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Symfony\Component\HttpFoundation\Session\Session;


$session = new Session();
$selectedStoreType = $session->get('selected_store_type');
if(!empty($selectedStoreType)){
  $this->selectedStoreType = $selectedStoreType;
}else{
  $this->selectedStoreType = 12;
}

class CartController extends Controller

{

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  function __construct()
  {

  }



  public function addToCart(Request $request)
  {


    $validator = \Validator::make($request->all(), [
      'food_serial' => 'required|integer',
      'food_count' => 'required|integer',
    ]);

    if ($validator->fails()) {
      $responseArr = [];
      $responseArr['message'] = $validator->errors();;
      $responseArr['token'] = '';
      return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
    }


    $response = [];

    //check food count
    $productUnitsInStock = DB::table('BASProduct')
        ->where('Serial',$request->food_serial)
        ->first();

    if ($productUnitsInStock === null){
        throw \Exception('Product not found');
    }

    $productUnitsInStock = $productUnitsInStock->ProductUnit;

    $session = new Session();
    $customerCartSession = $session->get('customer_cart_session');

    if(empty($customerCartSession)){
      $customerCartSession = [];
    }

    $customerCartSession[$request->food_serial] = $request->food_count;

    $session->set('customer_cart_session',$customerCartSession);


    $allCartElementsCount = 0;
    foreach($customerCartSession as $sessionItem){
      $allCartElementsCount += $sessionItem;
    }

    $response = [
      'response_code' => 200,
      'response_message' => '',
      'cart_items' => $allCartElementsCount,
    ];


    return $response;


  }



  public function showCartPage(Request $request){
    $session = new Session();
    $customerCartSession = $session->get('customer_cart_session');

    $cartItems = [];
    if(!empty($customerCartSession)){
      foreach($customerCartSession as $foodSerial=>$cartItemCount){

        $product = DB::table('BASProduct')
            ->where('Serial',$foodSerial)
            ->first();

        $cartItems[] = [
          'serial' => $foodSerial,
          'count' => $cartItemCount,
          'product' => $product
        ];
      }
    }


    return view('customer.pages.cart',compact('cartItems'));

  }



  public function storeCart(Request $request){

    $validator = \Validator::make($request->all(), [
      'waiter_password' => 'required|min:3|max:20',
      'order_description' => 'nullable|max:500',
    ]);


    if ($validator->fails()) {
      return redirect()->to(route('showCartPage'));
    }

    //TODO: should be changed: check dynamic password and redirect to cart with a message
    if($request->waiter_password!='123456'){
      return redirect()->to(route('showCartPage'));
    }

    //TODO: get cart session items + order_description and store in db

    return redirect()->to(route('showCartCustomersList'));

  }


  public function showCartCustomersList(Request $request){
    return view('customer.pages.cart_customers_list');
  }


  public function getCustomersList(Request $request){


    //TODO: add AND dbo.GetSance(InTime) In (2) statement


    $currentJalaliDatetime = \Morilog\Jalali\Jalalian::now()->toDateTimeString();
    $currentJalaliDatetimeArray = explode(" ",$currentJalaliDatetime);
    $currentJalaliDate = explode("/",$currentJalaliDatetimeArray[0]);
    $currentJalaliDate = $currentJalaliDate[0].$currentJalaliDate[1].$currentJalaliDate[2];

    $allRecordsCount = DB::select('SELECT count(*) AS all_records_count
                                  FROM FUNLog L
                                  INNER JOIN FUNCustomer CU ON L.CifRef = CU.Serial
                                  WHERE CmpFlg = 0 AND InDate >= '.$currentJalaliDate.'
                                  AND KeyStatus IN(1)');

    $allRecordsCount = $allRecordsCount[0]->all_records_count;


    $filteredCount = 0;
    $filteredRecordsInArray = [];

    $whereStatement = "";
    if(!empty($request->search['value']) && strlen($request->search['value'])>2){
      $whereStatement = "AND (L.Serial LIKE '%".$request->search['value']."%' OR L.FullName LIKE '%".$request->search['value']."%')";
    }


    $currentJalaliDatetime = \Morilog\Jalali\Jalalian::now()->toDateTimeString();
    $currentJalaliDatetimeArray = explode(" ",$currentJalaliDatetime);
    $currentJalaliDate = explode("/",$currentJalaliDatetimeArray[0]);
    $currentJalaliDate = $currentJalaliDate[0].$currentJalaliDate[1].$currentJalaliDate[2];

    $filteredRecords = DB::select("SELECT * FROM(
                                    SELECT L.FullName, L.Serial, CU.NationalCode, Seq = ROW_NUMBER() OVER (ORDER BY CU.Serial)
                                    FROM FUNLog L
                                    INNER JOIN FUNCustomer CU ON L.CifRef = CU.Serial
                                    WHERE CmpFlg = 0 AND InDate >= ".$currentJalaliDate."
                                    AND KeyStatus IN(1)
                                    ".$whereStatement."
                                  ) AS t
                                  WHERE Seq BETWEEN ".$request->start." AND ".($request->start+$request->length)." ORDER BY FullName");


    $filteredCountWithoutPagination = DB::select("SELECT * FROM(
                                    SELECT L.FullName, CU.Serial
                                    FROM FUNLog L
                                    INNER JOIN FUNCustomer CU ON L.CifRef = CU.Serial
                                    WHERE CmpFlg = 0 AND InDate >= ".$currentJalaliDate."
                                    AND KeyStatus IN(1)
                                    ".$whereStatement."
                                  ) AS t");

      $filteredCount = count($filteredCountWithoutPagination);

      $customerBaseDir = 'uploads/personel/';

      $filteredRecordsInArray = [];
      foreach($filteredRecords as $filteredRecordItem){

        $customerFileDir = asset('template/img/default-user.png'); //default customer image
        if(!empty($filteredRecordItem->NationalCode) && file_exists(public_path($customerBaseDir.$filteredRecordItem->NationalCode.'.jpg'))){
          $customerFileDir = asset($customerBaseDir.$filteredRecordItem->NationalCode.'.jpg');
        }

        $filteredRecordsInArray[] = [$filteredRecordItem->FullName, $filteredRecordItem->Serial,'<img class="customers-list-default-user-img" src="'.$customerFileDir.'" alt="تصویر مشتری">'];
      }


    $response = [
      'draw' => $request->draw,
      'recordsTotal' => $allRecordsCount,
      'recordsFiltered' => $filteredCount,
      'data' => $filteredRecordsInArray
    ];


    return $response;

  }



  public function resetCart(){
    $session = new Session();
    $session->set('customer_cart_session',[]);


    return redirect()->to(route('showMainPage'));
  }



  public function finalStoreCart(Request $request){

    $validator = \Validator::make($request->all(), [
      'customer_serial' => 'required|integer', //is consider as log_ref
      'desk_number' => 'nullable|integer',
      'serve_time' => 'nullable|max:10',
    ]);

    $responseArr = [];
    if ($validator->fails()) {
      $responseArr['message'] = $validator->errors();;
      $responseArr['token'] = '1000';
      return response()->json($responseArr, 400);
    }

    //TODO: validate order: check in-stock products


    $currentJalaliDatetime = \Morilog\Jalali\Jalalian::now()->toDateTimeString();
    $currentJalaliDatetimeArray = explode(" ",$currentJalaliDatetime);
    $currentJalaliDate = explode("/",$currentJalaliDatetimeArray[0]);
    $currentJalaliDate = $currentJalaliDate[0].$currentJalaliDate[1].$currentJalaliDate[2];

    $getUser = DB::select("SELECT L.FullName, CU.Serial
                            FROM FUNLog L
                            INNER JOIN FUNCustomer CU ON L.CifRef = CU.Serial
                            WHERE CmpFlg = 0 AND InDate >= ".$currentJalaliDate."
                            AND KeyStatus IN(1)
                            AND L.Serial = ".$request->customer_serial);

    //Validate customer: is active
    if(count($getUser)==0){
      $responseArr['message'] = 'کاربر فعال یافت نشد';
      $responseArr['token'] = '2000';
      return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
    }

    $activeUser = $getUser[0];


    $session = new Session();
    $customerCartSession = $session->get('customer_cart_session');

    $cartItems = [];
    if(!empty($customerCartSession)){
      foreach($customerCartSession as $foodSerial=>$cartItemCount){

        $product = DB::table('BASProduct')
            ->where('Serial',$foodSerial)
            ->first();

        $cartItems[] = [
          'serial' => $foodSerial,
          'count' => $cartItemCount,
          'product' => $product
        ];
      }
    }




    //TODO: reduce in-stock products count


    //store the cart in db
    $insertQuery = 'INSERT INTO FUNBill (CifRef, LogRef, ProductRef, ProductCount, TotalPrice, UserRef, BillStatus, Clear, RegisterDate, RegisterTime, ProducePlace, Done, deskno, Serve_Time) ';

    $registerDateTime = \Morilog\Jalali\Jalalian::now()->toDateTimeString();
    $registerDateTimeArr = explode(" ",$registerDateTime);
    $registerDateArr = explode("/",$registerDateTimeArr[0]);
    $registerTimeArr = explode(":",$registerDateTimeArr[1]);
    $registerDateStr = $registerDateArr[0].$registerDateArr[1].$registerDateArr[2];
    $registerTimeStr = $registerTimeArr[0].":".$registerTimeArr[1];

    foreach ($cartItems as $key => $cartItem) {
      $totalItemPrice = $cartItem['product']->Price*$cartItem['count'];

      if($key>0){
        $insertQuery .= ",";
      }else{
        $insertQuery .= " VALUES";
      }

      $insertQuery .= "(".$activeUser->Serial.",".$request->customer_serial.",".$cartItem["serial"].",".$cartItem["count"].",".$totalItemPrice.",'16','1','0','".$registerDateStr."','".$registerTimeStr."',$this->selectedStoreType,'0', '".$request->desk_number."', '".$request->serve_time."')";

    }

    $maxFunBill = DB::select("SELECT max(Serial) as max_serial
                            FROM FUNBill
                            WHERE CifRef = ".$activeUser->Serial);
    $beforeInsertMaxFunBill = $maxFunBill[0];

    $storeCart = DB::statement($insertQuery);
    if(!$storeCart){
      $responseArr['message'] = 'DB ERROR';
      $responseArr['token'] = '2000';
      return response()->json([], Response::HTTP_BAD_REQUEST);
    }


    $maxFunBill = DB::select("SELECT max(Serial) as max_serial
                            FROM FUNBill
                            WHERE CifRef = ".$activeUser->Serial);
    $maxFunBill = $maxFunBill[0];
    $minFunBill = ($beforeInsertMaxFunBill->max_serial)+1;



    //flush the cart
    $session = new Session();
    $session->set('customer_cart_session',[]);



    $responseHtml = "<div id='cart_result_container'>";
    $responseHtml .= "<div class='alert alert-success'>
                        سفارش زیر برای مشتری: ".$activeUser->FullName." با موفقیت ثبت شد.
                      </div>";

    $responseHtml .= "<table class='table table-striped table-hover'>";


    foreach ($cartItems as $key => $cartItem) {
      $responseHtml .= "<tr>";
      $responseHtml .= "<td>";
      $responseHtml .= "کد ".$cartItem['serial'];
      $responseHtml .= "</td>";
      $responseHtml .= "<td>";
      $responseHtml .= $cartItem['product']->ProductName;
      $responseHtml .= "</td>";
      $responseHtml .= "<td>";
      $responseHtml .= $cartItem['count']." عدد";
      $responseHtml .= "</td>";
      $responseHtml .= "</tr>";
    }

    $responseHtml .= "</table>";
    $responseHtml .= "</div>";

    $responsePrintButton = '<button id="printButton" type="button" onclick="printUserInvoice(this,'.$maxFunBill->max_serial.','.$minFunBill.','.$maxFunBill->max_serial.')" class="btn btn-danger py-sm-3 px-sm-5 me-3 animated slideInLeft float-end">چاپ سفارش</button>';


    $response = [
      'response_code' => 200,
      'response_message' => '',
      'cart_html' => $responseHtml,
      'print_button' => $responsePrintButton
    ];


    return $response;



  }




  public function updateCart(Request $request)
  {


    $validator = \Validator::make($request->all(), [
      'product_id' => 'required|integer',
      'food_count' => 'required|integer',
    ]);

    if ($validator->fails()) {
      $responseArr = [];
      $responseArr['message'] = $validator->errors();;
      $responseArr['token'] = '';
      return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
    }


    $response = [];

    //check food count
    $productUnitsInStock = DB::table('BASProduct')
        ->where('Serial',$request->product_id)
        ->first();

    if ($productUnitsInStock === null){
        throw \Exception('Product not found');
    }


    $session = new Session();
    $customerCartSession = $session->get('customer_cart_session');

    if(empty($customerCartSession)){
      $customerCartSession = [];
    }

    $customerCartSession[$request->product_id] = $request->food_count;

    $session->set('customer_cart_session',$customerCartSession);


    $allCartElementsCount = 0;
    foreach($customerCartSession as $sessionItem){
      $allCartElementsCount += $sessionItem;
    }

    $response = [
        'response_code' => 200,
        'response_message' => 'تعداد اقلام به '.$request->food_count.' تغییر پیدا کرد.',
    ];

    return $response;


  }



  public function removeFromCart(Request $request)
  {
    $validator = \Validator::make($request->all(), [
      'product_serial' => 'required|integer'
    ]);

    if ($validator->fails()) {
      $responseArr = [];
      $responseArr['message'] = $validator->errors();;
      $responseArr['token'] = '';
      return response()->json($responseArr, Response::HTTP_BAD_REQUEST);
    }


    $response = [];


    $session = new Session();
    $customerCartSession = $session->get('customer_cart_session');

    if(empty($customerCartSession)){
      $customerCartSession = [];
    }

    unset($customerCartSession[$request->product_serial]);

    $session->set('customer_cart_session',$customerCartSession);

    $response = [
        'response_code' => 200,
        'response_message' => '',
    ];

    return $response;

  }


}
