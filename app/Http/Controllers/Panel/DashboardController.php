<?php
namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Symfony\Component\HttpFoundation\Session\Session;

class DashboardController extends Controller

{

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */


  public $selectedStoreType;

  function __construct()
  {
    $session = new Session();
    $selectedStoreType = $session->get('selected_store_type');
    if(!empty($selectedStoreType)){
      $this->selectedStoreType = $selectedStoreType;
    }else{
      $this->selectedStoreType = 12;
    }
  }


  public function showPanelDashboard(){

    $billCustomerArray = $this->getOrdersData();

    return view('customer.pages.dashboard',compact('billCustomerArray'));
  }


  public function getOrdersList(){

    $billCustomerArray = $this->getOrdersData();

    return $billCustomerArray;
  }


  private function getOrdersData(){

    $registerDateTime = \Morilog\Jalali\Jalalian::now()->toDateTimeString();
    $registerDateTimeArr = explode(" ",$registerDateTime);
    $registerDateArr = explode("/",$registerDateTimeArr[0]);
    $registerDateStr = $registerDateArr[0].$registerDateArr[1].$registerDateArr[2];


    $allBillsBasedOnCustomers = DB::select('SELECT FUNBill.Serial, FUNLog.FullName, FUNBill.CifRef, FUNBill.ProductCount, FUNBill.TotalPrice, FUNBill.BillStatus, FUNBill.ProductRef, FUNBill.deskno, FUNBill.Serve_Time FROM FUNBill
                                            JOIN FUNCustomer ON FUNCustomer.Serial = FUNBill.CifRef
                                            JOIN FUNLog ON FUNLog.CifRef = FUNCustomer.Serial
                                            WHERE FUNBill.RegisterDate>='.$registerDateStr.' AND FUNBill.ProducePlace = '.$this->selectedStoreType.'
                                            AND FUNLog.KeyStatus = 1 AND FUNBill.Clear = 0 AND FUNLog.CmpFlg = 0 AND Funlog.InDate = FUNBill.RegisterDate
                                            ORDER BY FUNBill.BillStatus, FUNBill.RegisterTime');


    $billStatusTexts = [
      1=> '<span  class="badge bg-secondary">
            آماده‌سازی
            </span>',
      2=> '<span  class="badge bg-warning text-dark">
            آماده‌تحویل
            </span>',
      3=>'<span class="badge bg-success">
            تحویل شد
            </span'
    ];

    $billRowClasses = [
      1=> 'warning-row',
      2=> '',
      3=>'success-row'
    ];

    $billCustomerArray = [];
    foreach($allBillsBasedOnCustomers as $billCustomerItem){

        $customerFullname = !empty($billCustomerItem->FullName)?$billCustomerItem->FullName:'-';

        $getProductName = DB::select('SELECT ProductName FROM BASProduct WHERE Serial='.$billCustomerItem->ProductRef);
        $billProductName = !empty($getProductName[0])?$getProductName[0]->ProductName:'-';


        $billCustomerArray[] = [
                'bill_serial' => $billCustomerItem->Serial,
                'customer_fullname' => $customerFullname,
                'product_name' => $billProductName,
                'product_count' => $billCustomerItem->ProductCount,
                'product_price' => $billCustomerItem->TotalPrice,
                'bill_status' => $billStatusTexts[$billCustomerItem->BillStatus],
                'row_class' => $billRowClasses[$billCustomerItem->BillStatus],
                'deskno' => $billCustomerItem->deskno??'-',
                'ServeTime' => $billCustomerItem->Serve_Time??'-',
            ];

    }


    $maxFUNBillSerial = DB::select('SELECT MAX(FUNBill.Serial) as MAX_SERIAL FROM FUNBill
                                            JOIN FUNCustomer ON FUNCustomer.Serial = FUNBill.CifRef
                                            JOIN FUNLog ON FUNLog.CifRef = FUNCustomer.Serial
                                            WHERE FUNBill.RegisterDate>='.$registerDateStr.' AND FUNBill.ProducePlace = '.$this->selectedStoreType.'
                                            AND FUNLog.KeyStatus = 1 AND FUNBill.Clear = 0 AND FUNLog.CmpFlg = 0 AND Funlog.InDate = FUNBill.RegisterDate');

    return [
              'data' => $billCustomerArray,
              'latestOrderSerial'=>$maxFUNBillSerial[0]->MAX_SERIAL
            ];


  }







  public function updateBill(Request $request)
  {


    $validator = \Validator::make($request->all(), [
      'bill_id' => 'required|integer',
      'food_count' => 'required|integer',
    ]);

    if ($validator->fails()) {
      $responseArr = [];
      $responseArr['response_message'] = $validator->errors();;
      $responseArr['response_code'] = '';
      return response()->json($responseArr, 400);
    }


    $response = [];

    $bill = DB::table('FUNBill')
        ->where('Serial',$request->bill_id)
        ->first();

    if (empty($bill) || $bill->Done != 0){
        $responseArr = [];
        $responseArr['response_message'] = 'این سفارش انجام شده است';
        $responseArr['response_code'] = '4000';
        return response()->json($responseArr, 200);
    }

    $product = DB::table('BASProduct')
            ->where('Serial',$bill->ProductRef)
            ->first();



    DB::table('FUNBill')
        ->where('Serial', $request->bill_id)
        ->limit(1)
        ->update(
                array('ProductCount' => $request->food_count, 'TotalPrice' => ($request->food_count*$product->Price))
                );



    $response = [
        'response_code' => 200,
        'response_message' => '',
    ];


    return $response;


  }




  public function updateBillStatus(Request $request){

    $validator = \Validator::make($request->all(), [
      'bill_id' => 'required|integer',
      'bill_status' => 'required|integer',
    ]);

    if ($validator->fails()) {
      $responseArr = [];
      $responseArr['response_message'] = $validator->errors();;
      $responseArr['response_code'] = '';
      return response()->json($responseArr, 400);
    }


    $response = [];

    $bill = DB::table('FUNBill')
        ->where('Serial',$request->bill_id)
        ->first();

    if (empty($bill) || $bill->Done != 0){
        $responseArr = [];
        $responseArr['response_message'] = 'این سفارش انجام شده است';
        $responseArr['response_code'] = '4000';
        return response()->json($responseArr, 200);
    }




    DB::table('FUNBill')
        ->where('Serial', $request->bill_id)
        ->limit(1)
        ->update(
                array('BillStatus' => $request->bill_status)
            );

    $billStatusTexts = [
      1=> '<span  class="badge bg-secondary">
            آماده‌سازی
            </span>',
      2=> '<span  class="badge bg-warning text-dark">
            آماده‌تحویل
            </span>',
      3=>'<span class="badge bg-success">
            تحویل شد
            </span'
    ];

    $response = [
        'response_code' => 200,
        'new_value' => $billStatusTexts[$request->bill_status],
        'response_message' => '',
    ];


    return $response;

  }



  public function removeBillOrder(Request $request){


    $validator = \Validator::make($request->all(), [
      'bill_id' => 'required|integer'
    ]);

    if ($validator->fails()) {
      $responseArr = [];
      $responseArr['response_message'] = $validator->errors();;
      $responseArr['response_code'] = '';
      return response()->json($responseArr, 400);
    }


    $response = [];

    $bill = DB::table('FUNBill')
        ->where('Serial',$request->bill_id)
        ->first();

    if (empty($bill) || $bill->BillStatus == 3){
        $responseArr = [];
        $responseArr['response_message'] = 'این سفارش قابلیت حذف شدن را ندارد!';
        $responseArr['response_code'] = '4000';
        return response()->json($responseArr, 200);
    }




    DB::table('FUNBill')
        ->where('Serial', $request->bill_id)
        ->limit(1)
        ->delete();



    $response = [
        'response_code' => 200,
        'removed' => 1,
        'response_message' => 'سفارش با موفقیت حذف شد!',
    ];


    return $response;



  }




  public function getUserInvoice(Request $request){



    $validator = \Validator::make($request->all(), [
      'bill_id' => 'required|integer',
      'min_fun_bill_id' => 'nullable|integer',
      'max_fun_bill_id' => 'nullable|integer'
    ]);


    $registerDateTime = \Morilog\Jalali\Jalalian::now()->toDateTimeString();
    $registerDateTimeArr = explode(" ",$registerDateTime);
    $registerDateArr = explode("/",$registerDateTimeArr[0]);
    $registerDateStr = $registerDateArr[0].$registerDateArr[1].$registerDateArr[2];




    $billRecord = DB::select('SELECT * FROM FUNBill WHERE Serial='.$request->bill_id);
    $billCustomerSerial = $billRecord[0]->CifRef;


    $funBillCondition = "";
    if(!empty($request->min_fun_bill_id) && !empty($request->max_fun_bill_id)){
      $funBillCondition = " AND FUNBill.Serial>=".$request->min_fun_bill_id." AND FUNBill.Serial<=".$request->max_fun_bill_id;
    }

    $allBillsBasedOnCustomer = DB::select('SELECT FUNBill.Serial, FUNLog.FullName, FUNBill.CifRef, FUNBill.ProductCount, FUNBill.TotalPrice, FUNBill.BillStatus, FUNBill.ProductRef, FUNBill.deskno, FUNBill.Serve_Time FROM FUNBill
                                            JOIN FUNCustomer ON FUNCustomer.Serial = FUNBill.CifRef
                                            JOIN FUNLog ON FUNLog.CifRef = FUNCustomer.Serial
                                            WHERE FUNBill.RegisterDate>='.$registerDateStr.' AND FUNBill.ProducePlace = '.$this->selectedStoreType.' '.$funBillCondition.'
                                            AND FUNLog.KeyStatus = 1 AND FUNBill.Clear = 0 AND FUNLog.CmpFlg = 0 AND Funlog.InDate = FUNBill.RegisterDate
                                            AND FUNBill.CifRef = '.$billCustomerSerial.'
                                            ORDER BY FUNBill.BillStatus, FUNBill.RegisterTime');


    $invoiceNumber = DB::select('SELECT MIN(FUNBill.Serial) AS invoice_number FROM FUNBill WHERE FUNBill.CifRef = '.$billCustomerSerial.' GROUP BY FUNBill.CifRef');
    $invoiceNumber = $invoiceNumber[0]->invoice_number;


    $customerFullname = !empty($allBillsBasedOnCustomer[0]->FullName)?$allBillsBasedOnCustomer[0]->FullName:'-';

    $invoiceData = ['invoice_numver'=>$invoiceNumber, 'customer_fullname'=>$customerFullname, 'datetime'=>$registerDateTime, 'items'=>[] ];
    foreach($allBillsBasedOnCustomer as $billCustomerItem){



        $getProductName = DB::select('SELECT ProductName FROM BASProduct WHERE Serial='.$billCustomerItem->ProductRef);
        $billProductName = !empty($getProductName[0])?$getProductName[0]->ProductName:'-';


        $invoiceData['items'][] = [
                'product_name' => $billProductName,
                'product_count' => $billCustomerItem->ProductCount,
                'product_price' => $billCustomerItem->TotalPrice,
                'deskno' => $billCustomerItem->deskno??'-',
                'serve_time' => $billCustomerItem->Serve_Time??'-'
            ];

    }



    return $invoiceData;
  }


  public function setStoreType(Request $request){
    $storeType = 12;
    if(!empty($request->store_type)){
      $storeType = $request->store_type;
    }


    $session = new Session();

    $session->set('selected_store_type',$storeType);

    return redirect()->to(route('showMainPage'));

  }

  public function selectStoreType(Request $request){
    return view('customer.pages.select_store_type',[]);
  }



}
