<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('/customer/add_to_cart', [App\Http\Controllers\Customer\CartController::class, 'addToCart'])->name('customer.add_to_cart');
Route::get('/customer/get_customers_list', [App\Http\Controllers\Customer\CartController::class, 'getCustomersList'])->name('customer.get_list');

Route::post('/customer/final-store_cart', [App\Http\Controllers\Customer\CartController::class, 'finalStoreCart'])->name('customer.final-store-cart');



