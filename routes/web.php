<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::get('/', [App\Http\Controllers\Customer\HomeController::class, 'showMainPage'])->name('showMainPage');

Route::get('/cart', [App\Http\Controllers\Customer\CartController::class, 'showCartPage'])->name('showCartPage');

Route::get('/cart/reset', [App\Http\Controllers\Customer\CartController::class, 'resetCart'])->name('resetCart');

Route::post('/cart/store', [App\Http\Controllers\Customer\CartController::class, 'storeCart'])->name('storeCart');

Route::post('/cart/update', [App\Http\Controllers\Customer\CartController::class, 'updateCart'])->name('updateCart');

Route::post('/cart/remove', [App\Http\Controllers\Customer\CartController::class, 'removeFromCart'])->name('removeFromCart');

Route::get('/select-store-type', [App\Http\Controllers\Panel\DashboardController::class, 'selectStoreType'])->name('selectStoreType');

Route::get('/set-store-type', [App\Http\Controllers\Panel\DashboardController::class, 'setStoreType'])->name('setStoreType');

Route::get('/cart/customers/list', [App\Http\Controllers\Customer\CartController::class, 'showCartCustomersList'])->name('showCartCustomersList');

Route::group(['middleware' => ['auth']], function() {
  Route::get('/panel/dashboard', [App\Http\Controllers\Panel\DashboardController::class, 'showPanelDashboard'])->name('panel.dashboard');

  Route::post('/customer/update-bill-count', [App\Http\Controllers\Panel\DashboardController::class, 'updateBill'])->name('panel.update-bill-count');

  Route::post('/customer/update-bill-status', [App\Http\Controllers\Panel\DashboardController::class, 'updateBillStatus'])->name('panel.update-bill_status');

  Route::post('/customer/remove-bill-order', [App\Http\Controllers\Panel\DashboardController::class, 'removeBillOrder'])->name('panel.delete-bill-order');

  Route::get('/panel/get-orders-list', [App\Http\Controllers\Panel\DashboardController::class, 'getOrdersList'])->name('panel.getOrdersList');

  Route::post('/panel/get-user-invoice', [App\Http\Controllers\Panel\DashboardController::class, 'getUserInvoice'])->name('panel.getUserInvoice');


});


/*
Route::get('/', function () {
	$users = DB::table('dbo.FUNBill')->get();
	print_r($users); die();
	die("121212");
    //return view('welcome');
});
*/

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
