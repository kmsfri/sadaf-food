@extends('customer.common.master')
@section('additional_css')
  <link href="{{asset('template/css/dataTables.dataTables.css')}}?param=14" rel="stylesheet">
@stop
@section('additional_js_top')
@endsection
@section('main_content')


<!-- Navbar & Hero Start -->
<div class="container-xxl position-relative p-0">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0">
    <a href="" class="navbar-brand p-0">
      <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>مجموعه فرهنگی ورزشی صدف طلایی</h1>
      <!-- <img src="img/logo.png" alt="Logo"> -->
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
      <span class="fa fa-bars"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

      <div class="navbar-nav ms-auto py-0 pe-4 menu-item-container">
        <a href="{{route('showMainPage')}}" class="nav-item nav-link active">خانه</a>
    </div>
    <a href="" class="btn btn-primary py-2 px-4">رزرو میز</a>
  </div>
</nav>

<div class="container-xxl py-5 bg-dark hero-header">
  <div class="container text-center my-4 pt-2 pb-1">
    <h1 class="display-3 text-white mb-2 animated slideInDown">ورود به پنل</h1>
  </div>
</div>

</div>
<!-- Navbar & Hero End -->
<section id="login_container" class="h-100" style="background-color: #eee;">
  <div class="container h-100 py-5">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12">



        <div class="card">
            <div class="card-header">ورود</div>

            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="row mb-3">
                        <label for="username" class="col-md-3 col-form-label">نام کاربری</label>

                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="password" class="col-md-3 col-form-label">رمز عبور</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>


                    <div class="row mb-3">
                      <div class="col-md-12 col-form-label" style="text-align: center">
                        <button type="submit" class="btn btn-primary">
                            ورود
                        </button>
                      </div>
                    </div>


                </form>
            </div>
        </div>



      </div>
    </div>
  </div>
</section>






@endsection
