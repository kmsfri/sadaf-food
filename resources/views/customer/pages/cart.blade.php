@extends('customer.common.master')
@section('additional_css')
@stop
@section('additional_js_top')
@endsection
@section('main_content')


<!-- Navbar & Hero Start -->
<div class="container-xxl position-relative p-0">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0">
    <a href="" class="navbar-brand p-0">
      <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>مجموعه فرهنگی ورزشی صدف طلایی</h1>
      <!-- <img src="img/logo.png" alt="Logo"> -->
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
      <span class="fa fa-bars"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

      <div class="navbar-nav ms-auto py-0 pe-4 menu-item-container">
        <a href="{{route('showMainPage')}}" class="nav-item nav-link active">خانه</a>
    </div>
    <a href="" class="btn btn-primary py-2 px-4">رزرو میز</a>
  </div>
</nav>

<div class="container-xxl py-5 bg-dark hero-header">
  <div class="container text-center my-4 pt-2 pb-1">
    <h1 class="display-3 text-white mb-2 animated slideInDown">سبد خرید</h1>
  </div>
</div>

</div>
<!-- Navbar & Hero End -->



<section class="h-100" style="background-color: #eee;">
  <div class="container h-100 py-5">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-10">



        <div class="card rounded-3 mb-4">
          <div class="card-body">


            @foreach($cartItems as $cartItem)
             <div class="row d-flex justify-content-between align-items-center border-bottom customer-cart-row-cont" dir="rtl">
              <div class="col-md-6 col-lg-7 col-xl-7">
                <h5 class="d-flex justify-content-between cart-food-main-title">
                    <span>{{$cartItem['product']->ProductName}}</span>
                </h5>
              </div>
              <div class="col-md-5 col-lg-4 col-xl-4 d-flex">

                <div class="col-lg-12 col-md-12 select-food-quantity" dir="rtl">
                  <div class="number product_count_selector_cont" data-product_id="{{$cartItem['serial']}}">
                    <span class="reduce_count">-</span><input type="text" value="{{$cartItem['count']}}"/><span class="increase_count">+</span>
                  </div>
                </div>



              </div>
              <div class="col-md-1 col-lg-1 col-xl-1 text-end">
                <a href="#" onclick="removeFromCart(this,{{$cartItem['serial']}})" class="text-danger">حذف</a>
              </div>
            </div>
            @endforeach

          </div>
        </div>


        <div class="card mb-4">
          <div class="card-body p-4 d-flex flex-row">
            <div data-mdb-input-init class="form-outline flex-fill">
              <input type="text" autocomplete="off" form="get_password_form" name="order_description" id="form1" class="form-control form-control-md" value="" style="text-align: right" placeholder="توضیحات سفارش" />
            </div>
          </div>
        </div>

        <div class="card">
          <div class="card-body">
            <button type="button" class="btn btn-primary py-sm-3 px-sm-5 me-3 animated slideInLeft"  data-bs-toggle="modal" data-bs-target="#getPasswordModal">ثبت نهایی</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>



<div class="modal fade" id="getPasswordModal" tabindex="-1" aria-labelledby="getPasswordModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content rounded-0">
      <div class="modal-header" dir="rtl">
        <h5 class="modal-title" id="getPasswordModalLabel">لطفا پسورد خود را وارد کنید.</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body ps-0 pe-0">
        <div class="container">

          <form id="get_password_form" method="post" action="{{route('storeCart')}}">

            @csrf

            <div class="row">
              <div class="col-lg-12 col-md-12" dir="rtl">
                <input required type="password" name="waiter_password" class="form-control form-control-md" style="text-align: center; direction: ltr"  />
              </div>
            </div>

            <div class="row mt-3">
              <div class="col-lg-12 col-md-12 select-food-quantity" dir="rtl">
                <button type="submit" class="btn btn-primary add-to-cart-btn">
                  ثبت نهایی
                </button>

              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
</div>



@endsection


@section('additional_js_bottom')

<script>


$(document).ready(function() {
  $('.reduce_count').click(function () {
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();

    updateProductOrderCount(this,'reduce');

    return false;
  });
  $('.increase_count').click(function () {
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();


    updateProductOrderCount(this,'increase');

    return false;
  });


});

function addToCart(foodSerial,obj){
  var foodCount = $('#food_count_'+foodSerial).val();

  //set loading
  $(obj).prop('disabled', true);


  $.ajax({
    url: '{{route("customer.add_to_cart")}}',
    type: 'POST',
    cache:false,
    data: {"food_serial":foodSerial, "food_count":foodCount },
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
    success: function(data){

      //unset loading
      $(obj).prop('disabled', false);

      if(data.response_code==400){
        alert(data.response_message);
      }else if(data.response_code==200){
        if(data.cart_items>0){
          $('#cart_count_container').html('('+data.cart_items+')')
        }else{
          $('#cart_count_container').html('')
        }
      }

    },
    error: function(data){
      alert('اضافه کردن به سبد خرید با خطا مواجه شد');
      //unset loading
      $(obj).prop('disabled', false);
    }
  });

}



function updateProductOrderCount(obj, op_type){

    $(obj).closest('.product_count_selector_cont').find('input').prop('disabled', true);
    $(obj).closest('.product_count_selector_cont').find('.reduce_count').prop('disabled', true);
    $(obj).closest('.product_count_selector_cont').find('.increase_count').prop('disabled', true);

    var new_count_value = $(obj).closest('.product_count_selector_cont').find('input').val();
    var product_id = $(obj).closest('.product_count_selector_cont').data('product_id');

    $.ajax({
        url: '{{route("updateCart")}}',
        type: 'POST',
        cache:false,
        data: {"product_id":product_id,"food_count":new_count_value},
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        success: function(data){

            if(data.response_message!==''){
                alert(data.response_message);
            }

            //unset loading
            $(obj).closest('.product_count_selector_cont').find('input').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.reduce_count').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.increase_count').prop('disabled', false);

        },
        error: function(data){

            alert('ثبت با خطا مواجه شد!');

            //unset loading
            $(obj).closest('.product_count_selector_cont').find('input').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.reduce_count').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.increase_count').prop('disabled', false);

            if(op_type=='reduce'){

                $(obj).closest('.product_count_selector_cont').find('input').val((new_count_value+1));

            }else{

                $(obj).closest('.product_count_selector_cont').find('input').val((new_count_value-1));

            }



        }
    });

}

function removeFromCart(obj,product_serial){


    $.ajax({
        url: '{{route("removeFromCart")}}',
        type: 'POST',
        cache:false,
        data: {"product_serial":product_serial},
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        success: function(data){

            if(data.response_message!==''){
                alert(data.response_message);
            }

            $(obj).closest('.customer-cart-row-cont').remove();

        },
        error: function(data){

            alert('حذف با خطا مواجه شد!');

        }
    });

}


</script>

@endsection
