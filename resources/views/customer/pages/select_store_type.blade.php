@extends('customer.common.master')
@section('additional_css')
@stop
@section('additional_js_top')
@endsection
@section('main_content')


<!-- Navbar & Hero Start -->
<div class="container-xxl position-relative p-0">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0">
      <a href="{{route('showMainPage')}}" class="navbar-brand p-0">
        <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>مجموعه فرهنگی ورزشی صدف طلایی</h1>
        <!-- <img src="img/logo.png" alt="Logo"> -->
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
        <span class="fa fa-bars"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">

        <div class="navbar-nav ms-auto py-0 pe-4 menu-item-container">
          <a href="{{route('showMainPage')}}" class="nav-item nav-link active">خانه</a>
          <!--
          <a href="#" class="nav-item nav-link">سفارش جدید</a>
          <a href="#" class="nav-item nav-link">سبد خرید</a>
          <a href="#" class="nav-item nav-link">تایید</a>
        -->
      </div>
      <a href="" class="btn btn-primary py-2 px-4">رزرو میز</a>
    </div>
  </nav>

  <div class="container-xxl py-5 bg-dark hero-header mb-5 ">
      <div class="container my-5 py-5">
        <div class="row align-items-center g-5" dir="rtl">
          <div class="col-lg-6 text-center text-lg-start">
            <p class="text-white animated slideInLeft mb-4 pb-2 neonText">
              مجموعه فرهنگی ورزشی صدف طلایی
            </p>
            <!--
            <p class="text-white animated slideInLeft mb-4 pb-2">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
          -->
            <a href="{{route('setStoreType',['store_type'=>13])}}" class="btn btn-primary py-sm-3 px-sm-5 me-3 animated slideInLeft">بوفه</a>
            <a href="{{route('setStoreType',['store_type'=>12])}}" class="btn btn-info py-sm-3 px-sm-5 me-3 animated slideInLeft">رستوران</a>
        </div>
        <div class="col-lg-6 text-center text-lg-end overflow-hidden">
          <img class="img-fluid" src="{{asset('template/img/hero.png')}}" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Navbar & Hero End -->



@endsection
