@extends('customer.common.master')
@section('additional_css')
  <link href="{{asset('template/css/dataTables.dataTables.css')}}?param=15" rel="stylesheet">
  <style>
      .dashboard-bills-list-container table{
          direction: rtl;
      }

      .container,
      .container-xxl{
        max-width: 100%;
      }
  </style>
@stop
@section('additional_js_top')
@endsection
@section('main_content')


<!-- Navbar & Hero Start -->
<div class="container-xxl position-relative p-0">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0">
    <a href="" class="navbar-brand p-0">
      <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>مجموعه فرهنگی ورزشی صدف طلایی</h1>
      <!-- <img src="img/logo.png" alt="Logo"> -->
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
      <span class="fa fa-bars"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

      <div class="navbar-nav ms-auto py-0 pe-4 menu-item-container">
        <a href="{{route('showMainPage')}}" class="nav-item nav-link active">خانه</a>
    </div>
    <a href="" class="btn btn-primary py-2 px-4">رزرو میز</a>
  </div>
</nav>

<div class="container-xxl py-5 bg-dark hero-header">
  <div class="container text-center my-4 pt-2 pb-1">
    <h1 class="display-3 text-white mb-2 animated slideInDown">لیست سفارشات</h1>
  </div>
</div>

</div>
<!-- Navbar & Hero End -->



<section class="h-100" style="background-color: #eee;">
  <div class="container h-100 py-5">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12">



        <div class="card rounded-3 mb-4">
          <div class="card-body dashboard-bills-list-container">

            <table id="orders_list_table" class="display" style="width:100%">
              <thead>
                <tr>
                  <th>میز</th>
                  <th>ساعت سرو</th>
                  <th>مشتری</th>
                  <th>نام غذا</th>
                  <th>تعداد</th>
                  <th>وضعیت سفارش</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>

              <tbody id="orders-list-table-body">
                @foreach($billCustomerArray['data'] as $billItem)
                    <tr class="{{$billItem['row_class']}}">
                      <td>{{$billItem['deskno']}}</td>
                      <td>{{$billItem['ServeTime']}}</td>
                      <td>{{$billItem['customer_fullname']}}</td>
                      <td>{{$billItem['product_name']}}</td>
                      <td>

                          <div class="col-lg-12 col-md-12 select-food-quantity" dir="rtl">
                            <div class="number product_count_selector_cont" data-bill_id="{{$billItem['bill_serial']}}">
                              <span class="reduce_count">-</span><input type="text" value="{{$billItem['product_count']}}"/><span class="increase_count">+</span>
                            </div>
                          </div>


                      </td>
                      <td class='bill-status-column'>{!! $billItem['bill_status']!!}</td>
                      <td><button class="btn btn-danger btn-sm" onclick="deleteOrder(this,{{$billItem['bill_serial']}})">حذف</button></td>
                      <td><button class="btn btn-success btn-sm" onclick="changeOrderStatus(this,{{$billItem['bill_serial']}},'3')">تحویل شد</button></td>
                      <td><button class="btn btn-secondary btn-sm" onclick="changeOrderStatus(this,{{$billItem['bill_serial']}},'1')">آماده‌سازی</button></td>
                      <td><button class="btn btn-info btn-sm" onclick="printUserInvoice(this,{{$billItem['bill_serial']}},'1')">چاپ کل سفارش</button></td>
                    </tr>
                @endforeach
              </tbody>


            </table>


          </div>
        </div>



      </div>
    </div>
  </div>
</section>


<div class="toast-container position-fixed bottom-0 end-0 p-3">
  <div id="liveToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <img src="..." class="rounded me-2" alt="...">
      <strong class="me-auto">سفارش جدید</strong>
      <small>همین الان</small>
      <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body">
      یک سفارش جدید ثبت شده است!
    </div>
  </div>
</div>

<div class="hide">
  <div id="print_invoice">
  </div>
</div>

@endsection


@section('additional_js_bottom')
<script src="{{asset('template/js/dataTables.js')}}?param=1"></script>
<script>

new DataTable('#orders_list_table', {
  pageLength : 15,
  processing: false,
  serverSide: false,

  "language": {
        "sProcessing":    "در حال پردازش",
        "sLengthMenu":    "نمایش رکوردهای _MENU_",
        "sZeroRecords":   "نتیجه‌ای یافت نشد",
        "sEmptyTable":    "هیچ داده‌ای در این جدول موجود نیست",
        "sInfo":          "نمایش رکوردهای _START_ تا _END_ از مجموع _TOTAL_ رکورد",
        "sInfoEmpty":     "نمایش رکوردهای از ۰ تا ۰ از مجموع ۰ رکورد",
        "sInfoFiltered":  "(فیلتر کردن کل _MAX_ رکورد)",
        "sInfoPostFix":   "",
        "sSearch":        "جستجو:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "در حال دریافت...",
        "oPaginate": {
            "sFirst":    "اولین",
            "sLast":    "آخرین",
            "sNext":    "بعدی",
            "sPrevious": "قبلی"
        },
        "oAria": {
            "sSortAscending":  ":مرتب سازی صعودی",
            "sSortDescending": ":مرتب سازی نزولی"
        }
    }



});




$(document).ready(function() {
  $('body').on('click','.reduce_count',function(){
    var $input = $(this).parent().find('input');
    var count = parseInt($input.val()) - 1;
    count = count < 1 ? 1 : count;
    $input.val(count);
    $input.change();

    updateProductOrderCount(this,'reduce');

    return false;
  });
  $('body').on('click','.increase_count',function(){
    var $input = $(this).parent().find('input');
    $input.val(parseInt($input.val()) + 1);
    $input.change();


    updateProductOrderCount(this,'increase');

    return false;
  });


});

function updateProductOrderCount(obj, op_type){


    var bill_id = $(obj).closest('.product_count_selector_cont').data('bill_id');

    var new_count_value = $(obj).closest('.product_count_selector_cont').find('input').val();

    $(obj).closest('.product_count_selector_cont').find('input').prop('disabled', true);
    $(obj).closest('.product_count_selector_cont').find('.reduce_count').prop('disabled', true);
    $(obj).closest('.product_count_selector_cont').find('.increase_count').prop('disabled', true);

    var new_count_value = $(obj).closest('.product_count_selector_cont').find('input').val();
    var bill_id = $(obj).closest('.product_count_selector_cont').data('bill_id');

    $.ajax({
        url: '{{route("panel.update-bill-count")}}',
        type: 'POST',
        cache:false,
        data: {"bill_id":bill_id,"food_count":new_count_value},
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        success: function(data){

            if(data.response_message!==''){
                alert(data.response_message);
            }

            //unset loading
            $(obj).closest('.product_count_selector_cont').find('input').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.reduce_count').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.increase_count').prop('disabled', false);

        },
        error: function(data){

            alert('ثبت با خطا مواجه شد!');

            //unset loading
            $(obj).closest('.product_count_selector_cont').find('input').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.reduce_count').prop('disabled', false);
            $(obj).closest('.product_count_selector_cont').find('.increase_count').prop('disabled', false);

            if(op_type=='reduce'){

                $(obj).closest('.product_count_selector_cont').find('input').val((new_count_value+1));

            }else{

                $(obj).closest('.product_count_selector_cont').find('input').val((new_count_value-1));

            }



        }
    });

}




function changeOrderStatus(obj,bill_serial, new_status){

    $(obj).prop('disabled', true);

    $.ajax({
        url: '{{route("panel.update-bill_status")}}',
        type: 'POST',
        cache:false,
        data: {"bill_id":bill_serial,"bill_status":new_status},
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        success: function(data){

            if(data.response_message!==''){
                alert(data.response_message);
            }

            if(data.new_value !==''){

                //TODO: remove all previous classes from row
                $(obj).closest('tr').removeClass('success-row');
                  $(obj).closest('tr').removeClass('warning-row');

                if(new_status==3){
                  $(obj).closest('tr').addClass('success-row');
                }else if(new_status==1){
                  $(obj).closest('tr').addClass('warning-row');
                }
                $(obj).closest('tr').find('.bill-status-column').html(data.new_value);
            }

            $(obj).prop('disabled', false);

        },
        error: function(data){

            alert('ثبت با خطا مواجه شد!');

            $(obj).prop('disabled', false);

        }
    });
}



function deleteOrder(obj,bill_serial){

    if (!window.confirm("آیا مطمئنید؟")) {
      return false;
    }


    $(obj).prop('disabled', true);

    $.ajax({
        url: '{{route("panel.delete-bill-order")}}',
        type: 'POST',
        cache:false,
        data: {"bill_id":bill_serial},
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        success: function(data){

            if(data.response_message!==''){
                alert(data.response_message);
            }

            if(data.removed == 1){
                $(obj).closest('tr').remove();
            }


        },
        error: function(data){

            alert('عملیات با خطا مواجه شد!');

            $(obj).prop('disabled', false);

        }
    });
}

const toastLiveExample = document.getElementById('liveToast')
const toast = new bootstrap.Toast(toastLiveExample)
var latestOrderSerial = 0;

function getOrdersList(){

    $.ajax({
        url: '{{route("panel.getOrdersList")}}',
        type: 'GET',
        cache:false,
        data: {},
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        success: function(result){



          if(result.latestOrderSerial>latestOrderSerial && latestOrderSerial>0 ){
            //notify
            toast.show();
            playSound('{{asset("template/sounds/notification-sound.wav")}}');
          }

          latestOrderSerial = result.latestOrderSerial;

          var resultHTML = '';
          for (const orderRow  of result.data) {
              resultHTML += '<tr class="'+orderRow.row_class+'">';
              resultHTML += '<td>'+orderRow.deskno+'</td>';
              resultHTML += '<td>'+orderRow.ServeTime+'</td>';
              resultHTML += '<td>'+orderRow.customer_fullname+'</td>';
              resultHTML += '<td>'+orderRow.product_name+'</td>';
              resultHTML += '<td>\
                              <div class="col-lg-12 col-md-12 select-food-quantity" dir="rtl">\
                                <div class="number product_count_selector_cont" data-bill_id="'+orderRow.bill_serial+'">\
                                  <span class="reduce_count">-</span><input type="text" value="'+orderRow.product_count+'"/><span class="increase_count">+</span>\
                                </div>\
                              </div>\
                            </td>';
              resultHTML += '<td class="bill-status-column">'+orderRow.bill_status+'</td>';
              resultHTML += '<td><button class="btn btn-danger btn-sm" onclick="deleteOrder(this,'+orderRow.bill_serial+')">حذف</button></td>';
              resultHTML += '<td><button class="btn btn-success btn-sm" onclick="changeOrderStatus(this,'+orderRow.bill_serial+',3)">تحویل شد</button></td>';
              resultHTML += '<td><button class="btn btn-secondary btn-sm" onclick="changeOrderStatus(this,'+orderRow.bill_serial+',1)">آماده‌سازی</button></td>';
              resultHTML += '<td><button class="btn btn-info btn-sm" onclick="printUserInvoice(this,'+orderRow.bill_serial+',1)">چاپ کل سفارش</button></td>';
              resultHTML += '</tr>';
          }

          $('#orders-list-table-body').html(resultHTML);


        },
        error: function(data){

        }
    });
}


setInterval(function(){
    getOrdersList();
}, 30000)



function playSound(url) {
  const audio = new Audio(url);
  audio.play();
}


function printUserInvoice(obj,bill_serial){
  $.ajax({
      url: '{{route("panel.getUserInvoice")}}',
      type: 'POST',
      cache:false,
      data: {"bill_id":bill_serial},
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
      success: function(result){


        var printHtml = '\
          <style>\
            .print-container{\
              width: 100%;\
            }\
            .print-container table, .print-container td, .print-container th{\
              width: 100%;\
              color: black;\
              border: 1px solid !important;\
            }\
            .print-container table{\
              padding: 0 !important;\
            }\
            .print-container td, .print-container th{\
              padding: 5px;\
              margin: 0;\
            }\
            .print-container *{\
              text-align: right;\
              direction: rtl;\
            }\
            .print_product_name,\
            .print_product_count{\
              font-size: 18px;\
              font-weight: 700;\
            }\
          </style>\
          <div class="print-container">\
          <p>نام مشتری: '+result.customer_fullname+' <span style="float:left">شماره: '+result.invoice_numver+'</br>'+result.datetime+'</span></p>\
          </br>\
          <table>\
            <tr>\
              <th>آیتم</th>\
              <th>تعداد</th>\
              <th>شماره میز</th>\
              <th>ساعت سرو</th>\
            </tr>';




        for (const orderRow  of result.items) {
        printHtml = printHtml+'<tr>\
                                  <td class="print_product_name">'+orderRow.product_name+'</td>\
                                  <td class="print_product_count">'+orderRow.product_count+'</td>\
                                  <td>'+orderRow.deskno+'</td>\
                                  <td>'+orderRow.serve_time+'</td>\
                               </tr>';
        }

        printHtml = printHtml+"</table></div>";





        newWin = window.open("");
        newWin.document.write("<h3 align='center'>صورتحساب</h3>");
        newWin.document.write(printHtml);
        newWin.print();
        newWin.close();



      },
      error: function(data){

      }
  });
}



</script>

@endsection
