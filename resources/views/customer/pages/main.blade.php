@extends('customer.common.master')
@section('additional_css')
@stop
@section('additional_js_top')
@endsection
@section('main_content')


<!-- Navbar & Hero Start -->
<div class="container-xxl position-relative p-0">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0">
      <a href="{{route('showMainPage')}}" class="navbar-brand p-0">
        <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>مجموعه فرهنگی ورزشی صدف طلایی</h1>
        <!-- <img src="img/logo.png" alt="Logo"> -->
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
        <span class="fa fa-bars"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">

        <div class="navbar-nav ms-auto py-0 pe-4 menu-item-container">
          <a href="{{route('showMainPage')}}" class="nav-item nav-link active">خانه</a>
          <!--
          <a href="#" class="nav-item nav-link">سفارش جدید</a>
          <a href="#" class="nav-item nav-link">سبد خرید</a>
          <a href="#" class="nav-item nav-link">تایید</a>
        -->
      </div>
      <a href="" class="btn btn-primary py-2 px-4">رزرو میز</a>
    </div>
  </nav>

  <div class="container-xxl py-5 bg-dark hero-header mb-5 ">
      <div class="container my-5 py-5">
        <div class="row align-items-center g-5" dir="rtl">
          <div class="col-lg-6 text-center text-lg-start">
            <h1 class="display-3 text-white animated slideInLeft">از غذای خود<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;لذت ببرید</h1>
            <p class="text-white animated slideInLeft mb-4 pb-2 neonText">
              مجموعه فرهنگی ورزشی صدف طلایی
            </p>
            <!--
            <p class="text-white animated slideInLeft mb-4 pb-2">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
          -->
          <a href="" class="btn btn-primary py-sm-3 px-sm-5 me-3 animated slideInLeft">سفارش غذا</a>
        </div>
        <div class="col-lg-6 text-center text-lg-end overflow-hidden">
          <img class="img-fluid" src="{{asset('template/img/hero.png')}}" alt="">
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Navbar & Hero End -->




<!-- Reservation Start -->
<!-- Menu Start -->
<div class="pt-2 pb-5 main-course-container">
  <div class="">
    <div class="text-center wow fadeInUp" data-wow-delay="0.1s">
      <h5 class="section-title ff-secondary text-center text-primary fw-normal">Food Menu</h5>
    </div>


    <div class="tab-class text-center wow fadeInUp" data-wow-delay="0.1s">





      <ul class="nav nav-pills nav-fill border-bottom mb-2" id="top-menu-container" dir="rtl">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{route('showMainPage')}}">خانه</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('resetCart')}}" onclick="return confirm('آیا مطمئنید?')">سفارش جدید</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('showCartPage')}}">سبد خرید<span id="cart_count_container">{{($allCartElementsCount>0)?'('.$allCartElementsCount.')':''}}</span></a>
        </li>

      </ul>




      <div class="col-lg-3 col-md-3 col-sm-3 col-3 float-end side-menu-container"  style="text-align: right">
        <ul class="nav flex-column nav-pills d-inline-flex justify-content-center mb-5">

          @foreach($foodCategories as $key=>$foodCategory)

            <li class="nav-item">
              <a class="d-flex align-items-center text-start pb-1 mt-4 main-menu-pills-item {{($key==0)?'active':''}}" dir="rtl" data-bs-toggle="pill" href="#tab-{{$foodCategory->Code}}">
                <div class="pe-3 main-menu-item-cont neonText2">
                  <h6 class="mt-n1 mb-0">{{$foodCategory->Title}}</h6>
                </div>
              </a>
            </li>


          @endforeach



        </ul>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9 col-9 carousel-container" style="border-right: solid thin #ffffff2e; padding-top: 10px; padding-bottom: 3px">



        <div class="tab-content">


          @foreach($foods as $productCategoryCode=>$categoryFoodItems)

          <div id="tab-{{$productCategoryCode}}" class="tab-pane fade show p-0 {{($foodCategories->first()->Code==$productCategoryCode)?'active':''}}">



            <div id="demo{{$productCategoryCode}}" class="carousel slide" data-interval="false">


              <!-- Indicators/dots -->
              <div class="carousel-indicators" style="position: absolute;bottom: 0;top: 100%;">
                @foreach($categoryFoodItems as $key=>$foodItem)
                  <button type="button" data-bs-target="#demo{{$productCategoryCode}}" data-bs-slide-to="{{$key}}" class="{{($key==0)?'active':''}}"></button>

                @endforeach
              </div>

              <!-- The slideshow/carousel -->
              <div class="carousel-inner">



                @foreach($categoryFoodItems as $key=>$foodItem)
                <div class="carousel-item {{($key==0)?'active':''}}">


                  <div class="container">
                    <div class="row" dir="rtl">

                      <div class="col-lg-12 col-md-12 image-container" dir="rtl">
                        <img class="flex-shrink-0 img-fluid rounded img-food-big1" src="{{asset(!empty($foodItem->ProductPic)?'uploads/foods/'.$foodItem->ProductPic:'template/img/default-food4.png')}}" alt="" >
                        @if(!$foodItem->Supply)
                        <div class="freeShippingRibbon">ناموجود</div>
                        @endif
                      </div>
                      <div class="col-lg-8 col-md-12" dir="rtl">
                        <h5 class="d-flex justify-content-between food-item-title-cont">
                          <span>{{$foodItem->ProductName}}</span>

                        </h5>
                      </div>
                      @if($foodItem->Supply)
                      <div class="col-lg-4 col-md-12" dir="rtl">
                        <div class="select-food-quantity" dir="rtl" style="text-align: right">
                          <div class="number">
                            <span class="minus ">-</span><input autocomplete="off" id="food_count_{{$foodItem->Serial}}" type="text" value="{{(!empty($customerCartSession[$foodItem->Serial]))?$customerCartSession[$foodItem->Serial]:1}}"/><span class="plus">+</span>
                          </div>

                          <button type="button" class="btn btn-primary add-to-cart-btn" onclick="addToCart('{{$foodItem->Serial}}',this)">
                            <i class="fas fa-shopping-cart "></i>
                            افزودن
                          </button>

                        </div>
                      </div>
                      @endif
                      <div class="col-lg-12 col-md-12" dir="rtl">
                        <h5 class="d-flex justify-content-between" style="text-align: justify; color: #fff">

                          <small class="fst-italic">
                            <!--
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در س
                          -->
                          </small>
                        </h5>
                      </div>
                    </div>
                  </div>



                </div>
                @endforeach

              </div>

              <!-- Left and right controls/icons -->

              <button class="carousel-control-prev" type="button" data-bs-target="#demo{{$productCategoryCode}}" data-bs-slide="prev">
                <span class="carousel-control-prev-icon"></span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#demo{{$productCategoryCode}}" data-bs-slide="next">
                <span class="carousel-control-next-icon"></span>
              </button>

            </div>



          </div>



          @endforeach


      </div>



    </div>

  </div>
</div>
</div>
<!-- Menu End -->
<!-- Reservation Start -->


@endsection


@section('additional_js_bottom')

<script>

function addToCart(foodSerial,obj){
  var foodCount = $('#food_count_'+foodSerial).val();

  //set loading
  $(obj).prop('disabled', true);


  $.ajax({
    url: '{{route("customer.add_to_cart")}}',
    type: 'POST',
    cache:false,
    data: {"food_serial":foodSerial, "food_count":foodCount },
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
    success: function(data){

      //unset loading
      $(obj).prop('disabled', false);

      if(data.response_code==400){
        alert(data.response_message);
      }else if(data.response_code==200){
        if(data.cart_items>0){
          $('#cart_count_container').html('('+data.cart_items+')')
        }else{
          $('#cart_count_container').html('')
        }
      }

    },
    error: function(data){
      alert('اضافه کردن به سبد خرید با خطا مواجه شد');
      //unset loading
      $(obj).prop('disabled', false);
    }
  });

}


</script>

@endsection
