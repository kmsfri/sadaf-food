@extends('customer.common.master')
@section('additional_css')
  <link href="{{asset('template/css/dataTables.dataTables.css')}}?param=14" rel="stylesheet">
@stop
@section('additional_js_top')
@endsection
@section('main_content')


<!-- Navbar & Hero Start -->
<div class="container-xxl position-relative p-0">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0">
    <a href="" class="navbar-brand p-0">
      <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>مجموعه فرهنگی ورزشی صدف طلایی</h1>
      <!-- <img src="img/logo.png" alt="Logo"> -->
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
      <span class="fa fa-bars"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">

      <div class="navbar-nav ms-auto py-0 pe-4 menu-item-container">
        <a href="{{route('showMainPage')}}" class="nav-item nav-link active">خانه</a>
    </div>
    <a href="" class="btn btn-primary py-2 px-4">رزرو میز</a>
  </div>
</nav>

<div class="container-xxl py-5 bg-dark hero-header">
  <div class="container text-center my-4 pt-2 pb-1">
    <h1 class="display-3 text-white mb-2 animated slideInDown">انتخاب مشتری</h1>
  </div>
</div>

</div>
<!-- Navbar & Hero End -->



<section class="h-100" style="background-color: #eee;">
  <div class="container h-100 py-5">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12">



        <div class="card rounded-3 mb-4">
          <div class="card-body">

            <table id="customers_list_table" class="display" style="width:100%">
              <thead>
                <tr>
                  <th>نام و نام خانوادگی</th>
                  <th>کد</th>
                  <th>تصویر</th>
                </tr>
              </thead>
              <!--
              <tfoot>
                <tr>
                  <th>First name</th>
                  <th>Last name</th>
                  <th>Position</th>
                  <th>Office</th>
                  <th>Start date</th>
                  <th>Salary</th>
                </tr>
              </tfoot>
            -->
            </table>


          </div>
        </div>


        <div class="card">
          <div class="card-body">
            <button id="submitCartBtn" type="button" class="btn btn-primary py-sm-3 px-sm-5 me-3 animated slideInLeft"  data-bs-toggle="modal" data-bs-target="#getDeskNumberModal">ثبت نهایی</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>


<div class="modal fade" id="getDeskNumberModal" tabindex="-1" aria-labelledby="getDeskNumberModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content rounded-0">
      <div class="modal-header" dir="rtl">
        <h5 class="modal-title" id="getDeskNumberModalLabel">لطفا شماره میز و ساعت سرو را انتخاب کنید.</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body ps-0 pe-0">
        <div class="container">

          <form id="get_password_form" method="post" action="{{route('storeCart')}}">

            @csrf

            <div class="row">
              <div class="col-lg-12 col-md-12" dir="rtl">
                <label for="DeskNumber" class="form-label">شماره میز:</label>
                <select class="form-select" id="DeskNumber">
                  <option value="" selected disabled>انتخاب کنید</option>
                  @for($i=1; $i<=30; $i++)
                    <option value="{{$i}}">میز {{$i}}</option>
                  @endfor
                </select>
              </div>
            </div>

            <div class="row mt-3">
              <div class="col-lg-12 col-md-12" dir="rtl">
                <label for="serve_time" class="form-label">ساعت سرو:</label>
                <input type="text" class="form-control" id="serve_time" placeholder="مثال: 10:10" name="text">
              </div>
            </div>

            <div class="row mt-3">
              <div class="col-lg-12 col-md-12 select-food-quantity" dir="rtl">
                <button id="submitCartBtn2" type="button" class="btn btn-primary add-to-cart-btn">
                  ثبت نهایی
                </button>

              </div>
            </div>

          </form>

        </div>
      </div>
    </div>
  </div>
</div>



@endsection


@section('additional_js_bottom')
<script src="{{asset('template/js/dataTables.js')}}?param=1"></script>
<script>

new DataTable('#customers_list_table', {
  pageLength : 15,
  ajax: '{{route("customer.get_list")}}',
  processing: true,
  serverSide: true,

  "language": {
        "sProcessing":    "در حال پردازش",
        "sLengthMenu":    "نمایش رکوردهای _MENU_",
        "sZeroRecords":   "نتیجه‌ای یافت نشد",
        "sEmptyTable":    "هیچ داده‌ای در این جدول موجود نیست",
        "sInfo":          "نمایش رکوردهای _START_ تا _END_ از مجموع _TOTAL_ رکورد",
        "sInfoEmpty":     "نمایش رکوردهای از ۰ تا ۰ از مجموع ۰ رکورد",
        "sInfoFiltered":  "(فیلتر کردن کل _MAX_ رکورد)",
        "sInfoPostFix":   "",
        "sSearch":        "جستجو:",
        "sUrl":           "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "در حال دریافت...",
        "oPaginate": {
            "sFirst":    "اولین",
            "sLast":    "آخرین",
            "sNext":    "بعدی",
            "sPrevious": "قبلی"
        },
        "oAria": {
            "sSortAscending":  ":مرتب سازی صعودی",
            "sSortDescending": ":مرتب سازی نزولی"
        }
    }



});


const table = new DataTable('#customers_list_table');

table.on('click', 'tbody tr', (e) => {
    let classList = e.currentTarget.classList;

    if (classList.contains('selected')) {
        classList.remove('selected');
    }
    else {
        table.rows('.selected').nodes().each((row) => row.classList.remove('selected'));
        classList.add('selected');
    }
});

/*
document.querySelector('#submitCartBtn').addEventListener('click', function () {
});
*/

document.querySelector('#submitCartBtn2').addEventListener('click', function () {

    var deskNumber = $('#DeskNumber').val();7


    var serveTime = $('#serve_time').val();7

    var selectedRow =  table.row('.selected').data();
    if(selectedRow == null || selectedRow==''){
      alert('لطفا یک مشتری را انتخاب کنید.');
      return false;
    }

    var customerSerialNumber = selectedRow[1];

    //table.row('.selected').remove().draw(false);

    $(this).prop('disabled', true);

    $.ajax({
        url: '{{route("customer.final-store-cart")}}',
        type: 'POST',
        cache:false,
        data: {"customer_serial":customerSerialNumber, 'desk_number': deskNumber, 'serve_time': serveTime},
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
        success: function(data){

          if(data.response_code==200){
            $('#customers_list_table_wrapper').html(data.cart_html)
            $('#getDeskNumberModal .select-food-quantity').append(data.print_button);
          }
          //unset loading
          $('#submitCartBtn').prop('disabled', true);

        },
        error: function(data){

          $('#customers_list_table_wrapper').html("")

          alert('ثبت نهایی با خطا مواجه شد.');
          //unset loading
          $('#submitCartBtn').prop('disabled', false);
        }
      });


});



function printUserInvoice(obj,bill_serial, min_fun_bill_id, max_fun_bill_id){
  $.ajax({
      url: '{{route("panel.getUserInvoice")}}',
      type: 'POST',
      cache:false,
      data: {"bill_id":bill_serial,"min_fun_bill_id":min_fun_bill_id,"max_fun_bill_id":max_fun_bill_id},
      headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
      success: function(result){


        var printHtml = '\
          <style>\
            .print-container{\
              width: 100%;\
            }\
            .print-container table, .print-container td, .print-container th{\
              width: 100%;\
              color: black;\
              border: 1px solid !important;\
            }\
            .print-container table{\
              padding: 0 !important;\
            }\
            .print-container td, .print-container th{\
              padding: 5px;\
              margin: 0;\
            }\
            .print-container *{\
              text-align: right;\
              direction: rtl;\
            }\
            .print_product_name,\
            .print_product_count{\
              font-size: 18px;\
              font-weight: 700;\
            }\
          </style>\
          <div class="print-container">\
          <p>نام مشتری: '+result.customer_fullname+' <span style="float:left">شماره: '+result.invoice_numver+'</br>'+result.datetime+'</span></p>\
          </br>\
          <table>\
            <tr>\
              <th>آیتم</th>\
              <th>تعداد</th>\
              <th>شماره میز</th>\
              <th>ساعت سرو</th>\
            </tr>';




        for (const orderRow  of result.items) {
        printHtml = printHtml+'<tr>\
                                  <td class="print_product_name">'+orderRow.product_name+'</td>\
                                  <td class="print_product_count">'+orderRow.product_count+'</td>\
                                  <td>'+orderRow.deskno+'</td>\
                                  <td>'+orderRow.serve_time+'</td>\
                               </tr>';
        }

        printHtml = printHtml+"</table></div>";





        newWin = window.open("");
        newWin.document.write("<h3 align='center'>صورتحساب</h3>");
        newWin.document.write(printHtml);
        newWin.print();
        newWin.close();



      },
      error: function(data){

      }
  });
}
</script>

@endsection
