<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>کافه رستوران</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicon -->
  <link href="img/favicon.ico" rel="icon">

  <!-- Libraries Stylesheet -->
  <link href="{{asset('template/lib/animate/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('template/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
  <link href="{{asset('template/lib/tempusdominus/css/tempusdominus-bootstrap-4.min.css')}}" rel="stylesheet" />

  <!-- Customized Bootstrap Stylesheet -->
  <link href="{{asset('template/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Template Stylesheet -->
  <link href="{{asset('template/css/style.css')}}?param=26" rel="stylesheet">

  <meta name="_token" content="{!! csrf_token() !!}"/>

  @yield('additional_css')

  @yield('additional_js_top')

</head>

<body>
  <div class="container-xxl bg-white p-0">
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
      <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>
    <!-- Spinner End -->


    @yield('main_content')


    <div class="modal fade" id="selectFoodModal" tabindex="-1" aria-labelledby="selectFoodModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content rounded-0">
          <div class="modal-header" dir="rtl">
            <h5 class="modal-title" id="selectFoodModalLabel">انتخاب غذا</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body ps-0 pe-0">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 col-md-12" dir="rtl">
                  <h5 class="d-flex justify-content-between">
                    <span>عنوان کباب یا سالاد یا نوشیدنی</span>
                    <span class="text-primary">۲۵۰۰۰۰ تومان</span>
                  </h5>

                  <img class="flex-shrink-0 img-fluid rounded" src="img/food-big1.jpg" alt="" style="width: 100%;">


                  <small class="fst-italic">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد.</small>

                </div>
              </div>

              <div class="row mt-3">
                <div class="col-lg-12 col-md-12 select-food-quantity" dir="rtl">
                  <div class="number">
                    <span class="minus">-</span><input type="text" value="1"/><span class="plus">+</span>
                  </div>


                  <button type="button" class="btn btn-primary add-to-cart-btn">
                    <i class="fas fa-shopping-cart "></i>
                    افزودن به سبد خرید
                  </button>

                </div>
              </div>



            </div>
          </div>
        </div>
      </div>
    </div>


<!-- Footer Start -->
<div class="container-fluid bg-dark text-light footer pt-5 mt-5 wow fadeIn" data-wow-delay="0.1s">
  <div class="container py-3" dir="rtl">
    <div class="row g-5">
      <div class="col-lg-3 col-md-6">
        <div class="footer-item-header-container">
          <h4 class="section-title ff-secondary text-start text-primary fw-normal">Company</h4>
        </div>
        <a class="btn btn-link" href="">درباره ما</a>
        <a class="btn btn-link" href="">تماس با ما</a>
        <a class="btn btn-link" href="">رزرو</a>
        <a class="btn btn-link" href="">حریم شخصی</a>
        <a class="btn btn-link" href="">قوانین و شرایط</a>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="footer-item-header-container">
          <h4 class="section-title ff-secondary text-start text-primary fw-normal">Contact</h4>
        </div>
        <p class="mb-2"><i class="fa fa-map-marker-alt ms-3"></i>تهران، میدان آرژانتین</p>
        <p class="mb-2"><i class="fa fa-phone-alt ms-3"></i>
          <span dir="ltr">
            +21-22323344
          </span>
        </p>
        <p class="mb-2"><i class="fa fa-envelope ms-3"></i>
          <span dir="ltr">
            info@example.com
          </span>
        </p>
        <div class="d-flex pt-2">
          <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-twitter"></i></a>
          <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-facebook-f"></i></a>
          <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-youtube"></i></a>
          <a class="btn btn-outline-light btn-social" href=""><i class="fab fa-linkedin-in"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="footer-item-header-container">
          <h4 class="section-title ff-secondary text-start text-primary fw-normal">Opening</h4>
        </div>
        <h5 class="text-light fw-normal">شنبه تا پنج‌شنبه</h5>
        <p>۹ صبح تا ۹ شب</p>
        <h5 class="text-light fw-normal">جمعه</h5>
        <p>۱۱ صبح تا ۱۲ شب</p>
      </div>
      <div class="col-lg-3 col-md-6">
        <div class="footer-item-header-container">
          <h4 class="section-title ff-secondary text-start text-primary fw-normal">Newsletter</h4>
        </div>
        <p>لورم ایپسوم متن ساختگی با تولید سادگی.</p>
        <div class="position-relative mx-auto" style="max-width: 400px;">
          <input class="form-control border-primary w-100 py-3 ps-4 pe-5" type="text" placeholder="ایمیل شما" style="text-align: left">
          <button type="button" class="btn btn-primary py-2 position-absolute top-0 end-0 mt-2 me-2">ثبت ایمیل</button>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- Footer End -->


<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
</div>

<!-- JavaScript Libraries -->
<script src="{{asset('template/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('template/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('template/lib/wow/wow.min.js')}}"></script>
<script src="{{asset('template/lib/easing/easing.min.js')}}"></script>
<script src="{{asset('template/lib/waypoints/waypoints.min.js')}}"></script>
<script src="{{asset('template/lib/counterup/counterup.min.js')}}"></script>
<script src="{{asset('template/lib/owlcarousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('template/lib/tempusdominus/js/moment.min.js')}}"></script>
<script src="{{asset('template/lib/tempusdominus/js/moment-timezone.min.js')}}"></script>
<script src="{{asset('template/lib/tempusdominus/js/tempusdominus-bootstrap-4.min.js')}}"></script>

<!-- Template Javascript -->
<script src="{{asset('template/js/main.js')}}?param=1"></script>

@yield('additional_js_bottom')

</body>

</html>
